package decoder;

import java.util.Arrays;
import java.util.Base64;

import decoder.exception.DecodingException;
import decoder.model.BreakDuration;
import decoder.model.SpliceInfoSection;
import decoder.model.SpliceInsert;
import decoder.model.SpliceTime;

/**
 * Implements a SCTE35 Encoding mechanism.
 */
public class Scte35Encoder {

    public static void main(String[] args) throws DecodingException {

        SpliceInfoSection s = new SpliceInfoSection();

        s.tableID = 252;// 8 bits

        s.sectionSyntaxIndicator = 0;// 1 bit
        s.privateIndicator = 0;// 1 bit
        s.reserved1 = 3;// 2bit
        s.sectionLength = 47;// 4 + 8 =12 bits

        s.protocolVersion = 0;// 8 bits

        s.encryptedPacket = 0;// 1 bit
        s.encryptionAlgorithm = 0;// 6 bit
        s.ptsAdjustment = 0;// 1 + 8 + 8 +8 +8 =33 bits

        s.cwIndex = 0;// 8 bits

        s.tier = 4095;// 8 + 4 = 12 bits
        s.spliceCommandLength = 20;// 4 + 8 =12 bits

        s.spliceCommandType = 5;// 8 bits

        SpliceInsert spliceInsert = new SpliceInsert();
        s.spliceInsert = spliceInsert;

        s.spliceInsert.spliceEventID = 1207959689;// 32 bits

        s.spliceInsert.spliceEventCancelIndicator = 0;// 1 bit
        s.spliceInsert.reserved1 = 0;// 7 bit

        s.spliceInsert.outOfNetworkIndicator = 1;// 1bit
        s.spliceInsert.programSpliceFlag = 1;// 1bit
        s.spliceInsert.durationFlag = 1;// 1bit Why it's 1, when in document
        // it's value is 0?*********************
        s.spliceInsert.spliceImmediateFlag = 0;// 1bit
        s.spliceInsert.reserved2 = 0;// 4 bit his should be 1 as per the
        // document.??******************

        SpliceTime sisp = new SpliceTime();
        s.spliceInsert.sisp = sisp;

        s.spliceInsert.sisp.timeSpecifiedFlag = 1;// 1bit-it should be 1 as per
        // the doc??
        s.spliceInsert.sisp.reserved1 = 0;// 6 bit- Why it's 0 ?The value in the
        // doc is 111111??
        s.spliceInsert.sisp.ptsTime = 7214908044l;// 33 bits
        // s.spliceInsert.sisp.reserved2=0; not exist in document ??

        s.spliceInsert.uniqueProgramID = 1;// 16 bits

        s.spliceInsert.availNum = 1;// 8 bit

        s.spliceInsert.availsExpected = 1;// 8 bits

        s.descriptorLoopLength = 10;// 16 bits
        s.alignmentStuffing = 0;// N x 8 don't know?

        s.eCRC32 = 0;// 32 bits

        s.CRC32 = 2040847422;// 32 bits

        BreakDuration breakDuration = new BreakDuration();
        s.spliceInsert.breakDuration = breakDuration;

        s.spliceInsert.breakDuration.autoReturn = 1;
        s.spliceInsert.breakDuration.duration = 5429424;
        s.spliceInsert.breakDuration.reserved1 = 0;

        System.out.println(Arrays.toString(encoder(s, 50)));// change lenght as
        // per the
        // binaryData
        // decoded array's
        // lenght
        System.out.println("The Equivalent Binary is :");
        byte[] encodedBytes = Base64.getEncoder().encode(encoder(s, 50));
        System.out.println("encoded=" + Arrays.toString(encodedBytes));
        String encodedBinaryDataValue = new String(encodedBytes);
        System.out.println(encodedBinaryDataValue);
    }

    public static byte[] encoder(SpliceInfoSection s, int byteArrayLenght) {
        byte[] actualByte = new byte[byteArrayLenght];// TODO size should be the
        // size of the
        // binaryData array? Need to check.
        int i1;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        long l1;
        long l2;
        long l3;
        int desptr;
        int bufptr;

        actualByte[0] = (byte) s.tableID;

        i1 = (s.sectionSyntaxIndicator >> 7) & 0x01;
        i2 = (s.privateIndicator >> 6) & 0x01;
        i3 = (s.reserved1 << 4) & 0x30;
        i4 = (s.sectionLength >> 8) & 0xff;
        i5 = i1 + i2 + i3 + i4;
        actualByte[1] = (byte) i5;
        actualByte[2] = (byte) s.sectionLength;

        actualByte[3] = (byte) s.protocolVersion;

        i1 = (s.encryptedPacket >> 7) & 0x01;
        i2 = (s.encryptionAlgorithm >> 1) & 0x3f;
        l1 = (s.ptsAdjustment >> 32) & 0x00001;
        l2 = i1 + i2 + l1;
        actualByte[4] = (byte) l2;

        l1 = (s.ptsAdjustment >> 24) & 0xff;
        actualByte[5] = (byte) l1;

        l2 = (s.ptsAdjustment >> 16) & 0xff;
        actualByte[6] = (byte) l2;

        l1 = (s.ptsAdjustment >> 8) & 0xff;
        actualByte[7] = (byte) l1;

        l2 = s.ptsAdjustment & 0xff;
        actualByte[8] = (byte) l2;

        actualByte[9] = (byte) s.cwIndex;

        i1 = (s.tier >> 4) & 0xff;
        actualByte[10] = (byte) i1;

        i1 = (s.tier & 0x0f) << 4;
        i2 = (s.spliceCommandLength >> 8) & 0x0f;
        i3 = i1 + i2;
        actualByte[11] = (byte) i3;

        i1 = s.spliceCommandLength & 0xff;
        actualByte[12] = (byte) i1;

        actualByte[13] = (byte) s.spliceCommandType;
        bufptr = 14;
        // Assuming that this is the splice insert as per the Scte-35 doc
        if (s.spliceCommandType == 0x05) {

            i1 = (s.spliceInsert.spliceEventID >> 24) & 0xff;
            actualByte[bufptr] = (byte) i1;
            bufptr++;
            i2 = (s.spliceInsert.spliceEventID >> 16) & 0xff;
            actualByte[bufptr] = (byte) i2;
            bufptr++;
            i3 = (s.spliceInsert.spliceEventID >> 8) & 0xff;
            actualByte[bufptr] = (byte) i3;
            bufptr++;
            i4 = s.spliceInsert.spliceEventID & 0xff;
            actualByte[bufptr] = (byte) i4;
            bufptr++;
            i1 = s.spliceInsert.spliceEventCancelIndicator & 0x80;
            i2 = s.spliceInsert.reserved1 + 0x7f;// all 1's;
            i3 = i1 + i2;
            actualByte[bufptr] = (byte) i3;
            bufptr++;

            i1 = (s.spliceInsert.outOfNetworkIndicator & 0x8f) << 7;
            i2 = (s.spliceInsert.programSpliceFlag & 0x4f) << 6;
            i3 = (s.spliceInsert.durationFlag & 0x2f) << 5;
            i4 = (s.spliceInsert.spliceImmediateFlag & 0xff) << 4;
            i5 = s.spliceInsert.reserved2 + 0X0f;
            i6 = i1 + i2 + i3 + i4 + i5;

            // actualByte[19] = (byte) i6;
            actualByte[bufptr] = (byte) i6;
            bufptr++;
            System.out.println("Flags OON=" + s.spliceInsert.outOfNetworkIndicator + " Prog="
                    + s.spliceInsert.programSpliceFlag + " Duration=" + s.spliceInsert.durationFlag + " Immediate="
                    + s.spliceInsert.spliceImmediateFlag + "\n");

            if (s.spliceInsert.sisp.timeSpecifiedFlag == 0) { // Need to
                // discuss..
                i1 = s.spliceInsert.sisp.timeSpecifiedFlag + 0x80;
            } else {
                i1 = (s.spliceInsert.sisp.timeSpecifiedFlag << 7) & 0x8f;
            }
            if (s.spliceInsert.sisp.reserved1 == 0) { // Need to discuss..
                i2 = s.spliceInsert.sisp.reserved1 + 0X7E;
            } else {
                i2 = (s.spliceInsert.sisp.reserved1 << 6) + 0x3E;
            }
            l1 = (s.spliceInsert.sisp.ptsTime >> 32) & 0x01;

            actualByte[bufptr] = (byte) (i1 + i2 + l1);

            if ((s.spliceInsert.programSpliceFlag == 1) && (s.spliceInsert.spliceImmediateFlag == 0)) {
                if ((actualByte[bufptr] & 0x080) != 0) {
                    // pts time calc
                    bufptr++;
                    l1 = (s.spliceInsert.sisp.ptsTime >> 24) & 0x0ff;
                    actualByte[bufptr] = (byte) l1;
                    bufptr++;
                    l2 = (s.spliceInsert.sisp.ptsTime >> 16) & 0x000ff;
                    actualByte[bufptr] = (byte) l2;
                    bufptr++;
                    l1 = (s.spliceInsert.sisp.ptsTime >> 8) & 0x00000ff;
                    actualByte[bufptr] = (byte) l1;
                    bufptr++;
                    l2 = s.spliceInsert.sisp.ptsTime & 0x0000000ff;
                    actualByte[bufptr] = (byte) l2;
                }
                bufptr++;
            }
            if (s.spliceInsert.durationFlag != 0) {
                l1 = (s.spliceInsert.breakDuration.autoReturn << 7) & 0x80;// 1
                // bits
                l2 = 0x7E; // 6 bit reserved, assuming all 1's
                l3 = (s.spliceInsert.breakDuration.duration >> 32) & 0x01;
                actualByte[bufptr] = (byte) (l1 + l2 + l3);
                bufptr++;
                if (s.spliceInsert.breakDuration.autoReturn != 0) {
                    System.out.println("Auto Return\n");
                }
                l1 = (s.spliceInsert.breakDuration.duration >> 24) & 0x0ff;
                actualByte[bufptr] = (byte) l1;
                bufptr++;
                l2 = (s.spliceInsert.breakDuration.duration >> 16) & 0xff;
                actualByte[bufptr] = (byte) l2;
                bufptr++;
                l1 = (s.spliceInsert.breakDuration.duration >> 8) & 0xff;
                actualByte[bufptr] = (byte) l1;
                bufptr++;
                l2 = (s.spliceInsert.breakDuration.duration) & 0xff;
                actualByte[bufptr] = (byte) l2;
                bufptr++;
                double bsecs = s.spliceInsert.breakDuration.duration;
                bsecs /= 90000.0;
                System.out.println(String.format("break duration = 0x%09x = %f seconds\n",
                        s.spliceInsert.breakDuration.duration, bsecs));
            }
            i1 = (s.spliceInsert.uniqueProgramID >> 8) & 0xff;
            actualByte[bufptr] = (byte) i1;
            bufptr++;
            i2 = s.spliceInsert.uniqueProgramID & 0xff;
            actualByte[bufptr] = (byte) i2;
            bufptr++;
            i3 = s.spliceInsert.availNum & 0xff;
            actualByte[bufptr] = (byte) i3;
            bufptr++;
            i4 = s.spliceInsert.availsExpected & 0xff;
            actualByte[bufptr] = (byte) i4;
            bufptr++;
        } else {// will handle 'Time Signal case here
            System.out.println("spliceCommandType is other than SPLICE_INSERT");
            throw new AssertionError("spliceCommandType is other than SPLICE_INSERT");
        }
        if (s.spliceCommandLength != 0x0fff) { // legacy check
            if (bufptr != (s.spliceCommandLength + 14)) {
                System.out.println("ERROR encoded command length " + bufptr + " not equal to specified command length "
                        + s.spliceCommandLength + "\n");
                // Some kind of error, or unknown command
                // bufptr = spliceInfoSection.spliceCommandLength + 14;
            }
        }
        i5 = (s.descriptorLoopLength >> 8) & 0xff;
        actualByte[bufptr] = (byte) i5;
        bufptr++;
        i1 = s.descriptorLoopLength & 0xff;
        actualByte[bufptr] = (byte) i1;
        bufptr++;
        desptr = bufptr;
        if (s.descriptorLoopLength > 0) {
            while ((bufptr - desptr) < s.descriptorLoopLength) {
                int tag = 0;// It can have value either 0 or 1 or 2. Not
                // sure how will get this. assuming 0 for now.
                int len = 8;// Not sure how will get this. assuming 8 for
                // now as per the available data.
                int identifier = 1129661769;// Not sure how will get this.
                // currently referring from
                // decoder code line : 325.
                actualByte[bufptr] = (byte) tag;
                bufptr++;
                actualByte[bufptr] = (byte) len;
                bufptr++;
                l1 = (identifier >> 24) & 0xff;
                actualByte[bufptr] = (byte) l1;
                bufptr++;
                l2 = (identifier >> 16) & 0xff;
                actualByte[bufptr] = (byte) l2;
                bufptr++;
                l1 = (identifier >> 8) & 0xff;
                actualByte[bufptr] = (byte) l1;
                bufptr++;
                l2 = identifier & 0xff;
                actualByte[bufptr] = (byte) l2;
                bufptr++;

                int availDesc = 309;// Not sure how will get this. assuming
                // 309 for now as per the available
                // data.
                i1 = (availDesc >> 24) & 0xff;
                actualByte[bufptr] = (byte) i1;
                bufptr++;
                i2 = (availDesc >> 16) & 0xff;
                actualByte[bufptr] = (byte) i2;
                bufptr++;
                i3 = (availDesc >> 8) & 0xff;
                actualByte[bufptr] = (byte) i3;
                bufptr++;
                i4 = availDesc & 0xff;
                actualByte[bufptr] = (byte) i4;
                bufptr++;
            }
        }

        if (bufptr != (s.descriptorLoopLength + desptr)) {
            int dlen = bufptr - desptr;
            System.out.println("ERROR encoded descriptor length " + dlen + " not equal to specified descriptor length "
                    + s.descriptorLoopLength + "\n");
            bufptr = desptr + s.descriptorLoopLength;
            System.out.println("SKIPPING REST OF THE COMMAND!!!!!!\n");
        } else {

            if (s.encryptedPacket != 0) {
                s.alignmentStuffing = 0;
                s.eCRC32 = 0;
            }

            i1 = (s.CRC32 >> 24) & 0xff;
            actualByte[bufptr] = (byte) i1;
            bufptr++;
            i2 = (s.CRC32 >> 16) & 0xff;
            actualByte[bufptr] = (byte) i2;
            bufptr++;
            i3 = (s.CRC32 >> 8) & 0xff;
            actualByte[bufptr] = (byte) i3;
            bufptr++;
            i4 = s.CRC32 & 0xff;
            actualByte[bufptr] = (byte) i4;
            System.out.println(String.format("CRC32 = 0x%08x\n", s.CRC32));
        }
        return Arrays.copyOf(actualByte, bufptr+1);
    }
}