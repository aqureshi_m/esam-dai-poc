package decoder.model;

/**
 * Created by andres.aguilar on 6/16/16.
 */
public class SpliceInfoSection {

    public int tableID;
    public int sectionSyntaxIndicator;
    public int privateIndicator;
    public int reserved1;
    public int sectionLength;
    public int protocolVersion;
    public int encryptedPacket;
    public int encryptionAlgorithm;
    public long ptsAdjustment;
    public int cwIndex;
    public int tier;
    public int spliceCommandLength;
    public int spliceCommandType;
    public int descriptorLoopLength;
    public int alignmentStuffing;
    public int eCRC32;
    public int CRC32;
    public SpliceInsert spliceInsert;

    public int getTableID() {
        return tableID;
    }

    public void setTableID(int tableID) {
        this.tableID = tableID;
    }

    public int getSectionSyntaxIndicator() {
        return sectionSyntaxIndicator;
    }

    public void setSectionSyntaxIndicator(int sectionSyntaxIndicator) {
        this.sectionSyntaxIndicator = sectionSyntaxIndicator;
    }

    public int getPrivateIndicator() {
        return privateIndicator;
    }

    public void setPrivateIndicator(int privateIndicator) {
        this.privateIndicator = privateIndicator;
    }

    public int getReserved1() {
        return reserved1;
    }

    public void setReserved1(int reserved1) {
        this.reserved1 = reserved1;
    }

    public int getSectionLength() {
        return sectionLength;
    }

    public void setSectionLength(int sectionLength) {
        this.sectionLength = sectionLength;
    }

    public int getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(int protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    public int getEncryptedPacket() {
        return encryptedPacket;
    }

    public void setEncryptedPacket(int encryptedPacket) {
        this.encryptedPacket = encryptedPacket;
    }

    public int getEncryptionAlgorithm() {
        return encryptionAlgorithm;
    }

    public void setEncryptionAlgorithm(int encryptionAlgorithm) {
        this.encryptionAlgorithm = encryptionAlgorithm;
    }

    public long getPtsAdjustment() {
        return ptsAdjustment;
    }

    public void setPtsAdjustment(long ptsAdjustment) {
        this.ptsAdjustment = ptsAdjustment;
    }

    public int getCwIndex() {
        return cwIndex;
    }

    public void setCwIndex(int cwIndex) {
        this.cwIndex = cwIndex;
    }

    public int getTier() {
        return tier;
    }

    public void setTier(int tier) {
        this.tier = tier;
    }

    public int getSpliceCommandLength() {
        return spliceCommandLength;
    }

    public void setSpliceCommandLength(int spliceCommandLength) {
        this.spliceCommandLength = spliceCommandLength;
    }

    public int getSpliceCommandType() {
        return spliceCommandType;
    }

    public void setSpliceCommandType(int spliceCommandType) {
        this.spliceCommandType = spliceCommandType;
    }

    public int getDescriptorLoopLength() {
        return descriptorLoopLength;
    }

    public void setDescriptorLoopLength(int descriptorLoopLength) {
        this.descriptorLoopLength = descriptorLoopLength;
    }

    public int getAlignmentStuffing() {
        return alignmentStuffing;
    }

    public void setAlignmentStuffing(int alignmentStuffing) {
        this.alignmentStuffing = alignmentStuffing;
    }

    public int geteCRC32() {
        return eCRC32;
    }

    public void seteCRC32(int eCRC32) {
        this.eCRC32 = eCRC32;
    }

    public int getCRC32() {
        return CRC32;
    }

    public void setCRC32(int CRC32) {
        this.CRC32 = CRC32;
    }

    public SpliceInsert getSpliceInsert() {
        return spliceInsert;
    }

    public void setSpliceInsert(SpliceInsert spliceInsert) {
        this.spliceInsert = spliceInsert;
    }

    @Override
    public String toString() {
        return "SpliceInfoSection{" +
                "tableID=" + tableID +
                ", sectionSyntaxIndicator=" + sectionSyntaxIndicator +
                ", privateIndicator=" + privateIndicator +
                ", reserved1=" + reserved1 +
                ", sectionLength=" + sectionLength +
                ", protocolVersion=" + protocolVersion +
                ", encryptedPacket=" + encryptedPacket +
                ", encryptionAlgorithm=" + encryptionAlgorithm +
                ", ptsAdjustment=" + ptsAdjustment +
                ", cwIndex=" + cwIndex +
                ", tier=" + tier +
                ", spliceCommandLength=" + spliceCommandLength +
                ", spliceCommandType=" + spliceCommandType +
                ", descriptorLoopLength=" + descriptorLoopLength +
                ", alignmentStuffing=" + alignmentStuffing +
                ", eCRC32=" + eCRC32 +
                ", CRC32=" + CRC32 +
                ", spliceInsert=" + spliceInsert +
                '}';
    }
}
