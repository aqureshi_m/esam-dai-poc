package decoder.model;

/**
 * Created by andres.aguilar on 6/16/16.
 */
public class BreakDuration {

    public int autoReturn;
    public int reserved1;
    public long duration;

    public int getAutoReturn() {
        return autoReturn;
    }

    public void setAutoReturn(int autoReturn) {
        this.autoReturn = autoReturn;
    }

    public int getReserved1() {
        return reserved1;
    }

    public void setReserved1(int reserved1) {
        this.reserved1 = reserved1;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
