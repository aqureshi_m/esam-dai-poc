package decoder.model;

/**
 * Created by andres.aguilar on 6/16/16.
 */
public class SpliceInsert {

    public int spliceEventID;
    public int spliceEventCancelIndicator;
    public int reserved1;
    public int outOfNetworkIndicator;
    public int programSpliceFlag;
    public SpliceTime sisp = new SpliceTime();
    public int durationFlag;
    public int spliceImmediateFlag;
    public BreakDuration breakDuration = new BreakDuration();
    public int reserved2;
    public int uniqueProgramID;
    public int availNum;
    public int availsExpected;

    public int getSpliceEventID() {
        return spliceEventID;
    }

    public void setSpliceEventID(int spliceEventID) {
        this.spliceEventID = spliceEventID;
    }

    public int getSpliceEventCancelIndicator() {
        return spliceEventCancelIndicator;
    }

    public void setSpliceEventCancelIndicator(int spliceEventCancelIndicator) {
        this.spliceEventCancelIndicator = spliceEventCancelIndicator;
    }

    public int getReserved1() {
        return reserved1;
    }

    public void setReserved1(int reserved1) {
        this.reserved1 = reserved1;
    }

    public int getOutOfNetworkIndicator() {
        return outOfNetworkIndicator;
    }

    public void setOutOfNetworkIndicator(int outOfNetworkIndicator) {
        this.outOfNetworkIndicator = outOfNetworkIndicator;
    }

    public int getProgramSpliceFlag() {
        return programSpliceFlag;
    }

    public void setProgramSpliceFlag(int programSpliceFlag) {
        this.programSpliceFlag = programSpliceFlag;
    }

    public SpliceTime getSisp() {
        return sisp;
    }

    public void setSisp(SpliceTime sisp) {
        this.sisp = sisp;
    }

    public int getDurationFlag() {
        return durationFlag;
    }

    public void setDurationFlag(int durationFlag) {
        this.durationFlag = durationFlag;
    }

    public int getSpliceImmediateFlag() {
        return spliceImmediateFlag;
    }

    public void setSpliceImmediateFlag(int spliceImmediateFlag) {
        this.spliceImmediateFlag = spliceImmediateFlag;
    }

    public BreakDuration getBreakDuration() {
        return breakDuration;
    }

    public void setBreakDuration(BreakDuration breakDuration) {
        this.breakDuration = breakDuration;
    }

    public int getReserved2() {
        return reserved2;
    }

    public void setReserved2(int reserved2) {
        this.reserved2 = reserved2;
    }

    public int getUniqueProgramID() {
        return uniqueProgramID;
    }

    public void setUniqueProgramID(int uniqueProgramID) {
        this.uniqueProgramID = uniqueProgramID;
    }

    public int getAvailNum() {
        return availNum;
    }

    public void setAvailNum(int availNum) {
        this.availNum = availNum;
    }

    public int getAvailsExpected() {
        return availsExpected;
    }

    public void setAvailsExpected(int availsExpected) {
        this.availsExpected = availsExpected;
    }

    @Override
    public String toString() {
        return "SpliceInsert{" +
                "spliceEventID=" + spliceEventID +
                ", spliceEventCancelIndicator=" + spliceEventCancelIndicator +
                ", reserved1=" + reserved1 +
                ", outOfNetworkIndicator=" + outOfNetworkIndicator +
                ", programSpliceFlag=" + programSpliceFlag +
                ", sisp=" + sisp +
                ", durationFlag=" + durationFlag +
                ", spliceImmediateFlag=" + spliceImmediateFlag +
                ", breakDuration=" + breakDuration +
                ", reserved2=" + reserved2 +
                ", uniqueProgramID=" + uniqueProgramID +
                ", availNum=" + availNum +
                ", availsExpected=" + availsExpected +
                '}';
    }
}
