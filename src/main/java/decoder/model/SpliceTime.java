package decoder.model;

/**
 * Created by andres.aguilar on 6/16/16.
 */
public class SpliceTime {

    public int timeSpecifiedFlag;
    public int reserved1;
    public long ptsTime;
    public int reserved2;

    public int getTimeSpecifiedFlag() {
        return timeSpecifiedFlag;
    }

    public void setTimeSpecifiedFlag(int timeSpecifiedFlag) {
        this.timeSpecifiedFlag = timeSpecifiedFlag;
    }

    public int getReserved1() {
        return reserved1;
    }

    public void setReserved1(int reserved1) {
        this.reserved1 = reserved1;
    }

    public long getPtsTime() {
        return ptsTime;
    }

    public void setPtsTime(long ptsTime) {
        this.ptsTime = ptsTime;
    }

    public int getReserved2() {
        return reserved2;
    }

    public void setReserved2(int reserved2) {
        this.reserved2 = reserved2;
    }

    @Override
    public String toString() {
        return "SpliceTime{" +
                "timeSpecifiedFlag=" + timeSpecifiedFlag +
                ", reserved1=" + reserved1 +
                ", ptsTime=" + ptsTime +
                ", reserved2=" + reserved2 +
                '}';
    }
}
