package com.mobitv.esamdaipoc;

import cablelabs.iptvservices.esam.xsd.signal._1.ResponseSignalType;
import cablelabs.iptvservices.esam.xsd.signal._1.SignalProcessingEvent;
import cablelabs.iptvservices.esam.xsd.signal._1.SignalProcessingNotificationType;
import cablelabs.md.xsd.signaling._3.BinarySignalType;
import cablelabs.md.xsd.signaling._3.UTCPointDescriptorType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import decoder.Scte35Decoder;
import decoder.Scte35Encoder;
import decoder.exception.DecodingException;
import decoder.model.BreakDuration;
import decoder.model.SpliceInfoSection;
import decoder.model.SpliceInsert;
import decoder.model.SpliceTime;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Base64;

import static org.springframework.beans.BeanUtils.copyProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class EsamDaiPocApplication {

    public static void main(String[] args) {
        SpringApplication.run(EsamDaiPocApplication.class, args);
    }
}

@Log4j
@RestController
class EsamController {

    @Autowired
    SignalProperties properties;

    Scte35Encoder encoder = new Scte35Encoder();
    Scte35Decoder decoder = new Scte35Decoder(false);

    @PostMapping(value = "/mobi-platform-esam-service/platform/v5/esam/sps/scte35", produces = "application/xml", consumes = "application/xml")
    public String scte35(@RequestBody String data) throws DecodingException, JAXBException, JsonProcessingException, DatatypeConfigurationException {
        log.info(data);

        if (data.contains("SignalProcessingEvent")) {

            data = data.replaceAll("xmlns:md=\"http://www.cablelabs.com/namespaces/metadata/xsd/signaling/2\"",
                    "xmlns:md=\"urn:cablelabs:md:xsd:signaling:3.0\"");

            JAXBContext context = JAXBContext.newInstance(SignalProcessingEvent.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            SignalProcessingEvent signalProcessingEvent = (SignalProcessingEvent) unmarshaller
                    .unmarshal(new StringReader(data));
            SignalProcessingEvent.AcquiredSignal acquiredSignal = signalProcessingEvent.getAcquiredSignal().iterator().next();

            ObjectMapper mapper = new ObjectMapper();
            System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(signalProcessingEvent));

            SignalProcessingNotificationType signalProcessingNotificationType = new SignalProcessingNotificationType();
            signalProcessingNotificationType.setAcquisitionPointIdentity(acquiredSignal.getAcquisitionPointIdentity());

            byte[] bytes = acquiredSignal.getBinaryData().getValue();

            byte[] encodedBytes = Base64.getEncoder().encode(bytes);
            String encodedBinaryDataValue = new String(encodedBytes);
            SpliceInfoSection spliceInfoSection = decoder.base64Decode(encodedBinaryDataValue);

            if (properties.isVerbose()) {
                System.out.println("------- Request Splice Info Section Decoded: ");
                System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(spliceInfoSection));
            }

            if (properties.getUpdate().isSend()) {

                ResponseSignalType respSignal1 = new ResponseSignalType();
                respSignal1.setAcquisitionPointIdentity(acquiredSignal.getAcquisitionPointIdentity());
                respSignal1.setAcquisitionSignalID(acquiredSignal.getAcquisitionSignalID());
                respSignal1.setAction("replace");

                respSignal1.setUTCPoint(acquiredSignal.getUTCPoint());
                respSignal1.setStreamTimes(acquiredSignal.getStreamTimes());

                BinarySignalType t1 = new BinarySignalType();
                t1.setSignalType("SCTE35");
                byte[] bs = encoder.encoder(replace(spliceInfoSection, properties.getUpdate()), bytes.length);
                t1.setValue(Base64.getDecoder().decode(Base64.getEncoder().encode(bs)));
                respSignal1.setBinaryData(t1);

                signalProcessingNotificationType.getResponseSignal().add(respSignal1);
            }

            if (properties.getCreate().isSend()) {

                ResponseSignalType respSignal2 = new ResponseSignalType();
                respSignal2.setAcquisitionPointIdentity(acquiredSignal.getAcquisitionPointIdentity());
                respSignal2.setAcquisitionSignalID(acquiredSignal.getAcquisitionSignalID());
                respSignal2.setAction("create");

                DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
                UTCPointDescriptorType utcPoint = acquiredSignal.getUTCPoint();
                XMLGregorianCalendar calendar = (XMLGregorianCalendar) utcPoint.getUtcPoint().clone();
                calendar.add(datatypeFactory.newDuration(properties.getCreate().getNewSignalDelayInSec() * 1000));

                UTCPointDescriptorType utcPoint2 = new UTCPointDescriptorType();
                utcPoint2.setUtcPoint(calendar);

                respSignal2.setUTCPoint(utcPoint2);
                respSignal2.setStreamTimes(acquiredSignal.getStreamTimes());

                BinarySignalType t2 = new BinarySignalType();
                t2.setSignalType("SCTE35");
                byte[] bs = encoder.encoder(replace(spliceInfoSection, properties.getCreate()), bytes.length);
                t2.setValue(Base64.getDecoder().decode(Base64.getEncoder().encode(bs)));
                respSignal2.setBinaryData(t2);

                signalProcessingNotificationType.getResponseSignal().add(respSignal2);
            }

            if (!properties.getUpdate().isSend() && !properties.getCreate().isSend()) {
                ResponseSignalType respSignal2 = new ResponseSignalType();
                respSignal2.setAcquisitionPointIdentity(acquiredSignal.getAcquisitionPointIdentity());
                respSignal2.setAcquisitionSignalID(acquiredSignal.getAcquisitionSignalID());
                respSignal2.setAction("noop");
                respSignal2.setUTCPoint(acquiredSignal.getUTCPoint());
                respSignal2.setStreamTimes(acquiredSignal.getStreamTimes());

                BinarySignalType t2 = new BinarySignalType();
                t2.setSignalType("SCTE35");
                byte[] bs2 = encoder.encoder(spliceInfoSection, bytes.length);
                t2.setValue(Base64.getDecoder().decode(Base64.getEncoder().encode(bs2)));
                respSignal2.setBinaryData(t2);

                signalProcessingNotificationType.getResponseSignal().add(respSignal2);
            }

            JAXBContext context2 = JAXBContext.newInstance(SignalProcessingNotificationType.class);
            Marshaller marshaller = context2.createMarshaller();
            StringWriter writer = new StringWriter();
            marshaller.marshal(signalProcessingNotificationType, writer);

            String s = writer.toString();
            if (properties.isVerbose()) {
                System.out.println("------- Response XML: ");

                s = s.replaceAll("=\"urn:cablelabs:md:xsd:signaling:3.0\"",
                        "=\"http://www.cablelabs.com/namespaces/metadata/xsd/signaling/2\"");

                System.out.println(s);

                System.out.println("----------- Response JSON: ");
                System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(signalProcessingNotificationType));
            }

            return s;
        } else {
            return "";
        }
    }

    private SpliceInfoSection replace(SpliceInfoSection signal, SignalProperties.Signal properties) {
        SpliceInfoSection replaced = new SpliceInfoSection();
        copyProperties(signal, replaced);

        if (signal.spliceInsert != null) {
            SpliceInsert insert = new SpliceInsert();
            copyProperties(signal.spliceInsert, insert);

            SpliceTime sisp = new SpliceTime();
            copyProperties(signal.spliceInsert.sisp, sisp);
            insert.sisp = sisp;
            if (properties.getNewSignalDelayInSec() > 0) {
                insert.sisp.ptsTime += (properties.getNewSignalDelayInSec() * 90000);
            }

            BreakDuration duration = new BreakDuration();
            copyProperties(signal.spliceInsert.breakDuration, duration);
            insert.breakDuration = duration;

            replaced.spliceInsert = insert;
        }

        //modify
        replaced.spliceInsert.outOfNetworkIndicator = properties.isToggleOutOfNetwork() ?
                toggleBoolean(replaced.spliceInsert.outOfNetworkIndicator) :
                properties.isOutOfNetwork() == true ? 1 : 0;

        replaced.spliceInsert.breakDuration.autoReturn = properties.isToggleAutoReturn() ?
                toggleBoolean(replaced.spliceInsert.breakDuration.autoReturn) :
                properties.isAutoReturn() == true ? 1 : 0;

        return replaced;
    }

    private int toggleBoolean(int value) {
        return value == 1 ? 0 : 1;
    }

}

