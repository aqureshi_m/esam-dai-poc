package com.mobitv.esamdaipoc;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "signal")
public class SignalProperties {

    private Signal update;
    private Signal create;
    private boolean verbose;

    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public Signal getUpdate() {
        return update;
    }

    public void setUpdate(Signal update) {
        this.update = update;
    }

    public Signal getCreate() {
        return create;
    }

    public void setCreate(Signal create) {
        this.create = create;
    }

    public static class Signal {
        private boolean send;
        private boolean toggleOutOfNetwork;
        private boolean toggleAutoReturn;
        private boolean outOfNetwork;
        private boolean autoReturn;
        private int newSignalDelayInSec;

        public boolean isSend() {
            return send;
        }

        public void setSend(boolean send) {
            this.send = send;
        }

        public boolean isToggleOutOfNetwork() {
            return toggleOutOfNetwork;
        }

        public void setToggleOutOfNetwork(boolean toggleOutOfNetwork) {
            this.toggleOutOfNetwork = toggleOutOfNetwork;
        }

        public boolean isToggleAutoReturn() {
            return toggleAutoReturn;
        }

        public void setToggleAutoReturn(boolean toggleAutoReturn) {
            this.toggleAutoReturn = toggleAutoReturn;
        }

        public boolean isOutOfNetwork() {
            return outOfNetwork;
        }

        public void setOutOfNetwork(boolean outOfNetwork) {
            this.outOfNetwork = outOfNetwork;
        }

        public boolean isAutoReturn() {
            return autoReturn;
        }

        public void setAutoReturn(boolean autoReturn) {
            this.autoReturn = autoReturn;
        }

        public int getNewSignalDelayInSec() {
            return newSignalDelayInSec;
        }

        public void setNewSignalDelayInSec(int newSignalDelayInSec) {
            this.newSignalDelayInSec = newSignalDelayInSec;
        }
    }
}
