package com.mobitv.esamdaipoc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import decoder.Scte35Decoder;
import decoder.Scte35Encoder;
import decoder.exception.DecodingException;
import decoder.model.SpliceInfoSection;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Test;

import java.util.Base64;

public class EncoderTest {

    String b = "/DAvAAAAAAAAAP/wFAVIAACJf+//rgrCjP4AUtiwAAEBAQAKAAhDVUVJAAABNXmk3D4=";

    Scte35Decoder decoder = new Scte35Decoder(false);
    Scte35Encoder encoder = new Scte35Encoder();

    @Test
    public void test() throws DecodingException, JsonProcessingException {
        SpliceInfoSection section = decode(b.getBytes());

        print(section);
        System.out.println("-------");

        section.spliceInsert.outOfNetworkIndicator = section.spliceInsert.outOfNetworkIndicator == 1 ? 0 : 1;
        byte[] reEncodedBytes = encoder.encoder(section, b.getBytes().length);
        SpliceInfoSection reEncoded = decode(reEncodedBytes);

        print(reEncoded);

    }

    SpliceInfoSection decode(byte[] bytes) throws DecodingException {
        byte[] encodedBytes = Base64.getEncoder().encode(bytes);
        byte[] decoded = Base64.getDecoder().decode(encodedBytes);
        String encodedBinaryDataValue = new String(decoded);

        return decoder.base64Decode(encodedBinaryDataValue);
    }

    ObjectMapper m = new ObjectMapper();

    void print(Object o) throws JsonProcessingException {
        System.out.println(m.writerWithDefaultPrettyPrinter().writeValueAsString(o));
    }
}
