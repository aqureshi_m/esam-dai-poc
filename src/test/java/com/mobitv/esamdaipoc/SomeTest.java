package com.mobitv.esamdaipoc;

import decoder.exception.DecodingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, properties = "logging.level.org.springframework=INFO")
public class SomeTest {

    @Autowired
    EsamController controller;

    @Test
    public void test() throws IOException, JAXBException, DecodingException, DatatypeConfigurationException {

        List<String> strings = Files.readAllLines(Paths.get("src/test/resources/signal2.xml"), Charset.defaultCharset());
        String xml = strings.stream().collect(Collectors.joining("\n"));

        controller.scte35(xml);

    }
}
