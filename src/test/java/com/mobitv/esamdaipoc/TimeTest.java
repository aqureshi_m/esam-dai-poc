package com.mobitv.esamdaipoc;

import org.junit.Test;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.GregorianCalendar;

public class TimeTest {

    @Test
    public void test() throws DatatypeConfigurationException {
        DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar calendar = factory.newXMLGregorianCalendar(new GregorianCalendar());
        System.out.println(calendar);

        XMLGregorianCalendar  clone = (XMLGregorianCalendar) calendar.clone();
        calendar.add(factory.newDuration(30000));
        System.out.println(calendar);
    }
}
