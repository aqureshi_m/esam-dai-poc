package com.mobitv.esamdaipoc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import decoder.Scte35Decoder;
import decoder.exception.DecodingException;
import decoder.model.SpliceInfoSection;
import org.junit.Test;

import java.util.Base64;

public class ANotherTest {

    @Test
    public void test() throws DecodingException, JsonProcessingException {
        String b = "/DAgAAAAAAAAAP/wDwUAAABuf0//kKrm1gABAQEAAGwKlC4K";
        String b1 = "/DAvAAAAAAAAAP/wBQb/kat98gAZAhdDVUVJSAAA3n+fCAgAAAAAL/VnJjUAAKvoGNw=";

        byte[] bytes = b1.getBytes();

        byte[] encodedBytes = Base64.getEncoder().encode(bytes);
        String encodedBinaryDataValue = new String(encodedBytes);
        Scte35Decoder decoder = new Scte35Decoder(false);
        SpliceInfoSection spliceInfoSection = decoder.base64Decode(encodedBinaryDataValue);


        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(spliceInfoSection));
    }
}
